import { Routes, Route, BrowserRouter } from "react-router-dom";

import Header from './Header';
import Footer from './Footer';
import ProductPage from 'details/ProductPage';
import CartContent from 'cart/CartContent';
import HomePageContent from './HomePageContent'

const MainLayout = () =>{
return(
    <BrowserRouter>
    <div className="text-3xl mx-auto max-w-6xl">
      <Header />
      <div className="my-10">
        <Routes>
          <Route exact path="/" element={<HomePageContent />} />
          <Route path="/products/:id" element={<ProductPage />} />
          <Route path="/cart" element={<CartContent />} />
        </Routes>
      </div>
      <Footer />
    </div>
  </BrowserRouter>
  )
}
export default MainLayout;