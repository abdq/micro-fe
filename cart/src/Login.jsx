import { useState } from "react";
import { useLoggedIn , login } from "./cart";
import "./index.scss";

export default function Login(){

    const loggedIn = useLoggedIn();
    const [showLogin, setShowLogin] = useState(false);

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    if(loggedIn) return null;

    return(
        <>
        <span onClick={() => setShowLogin(!showLogin)}>
            <i className="ri-fingerprint-line text-2xl" id = "showLoggedIn"> </i>
        </span>
        {showLogin && (
            <div className=" bg-white absolute p-5 border-4 border-blue-800" style={{width: "300", top:"2rem"}}>
                <input 
                type="text"
                placeholder="User Name"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                className="border text-sm borger-gray-400 p-2 rounded-md w-full"
                />

                 <input 
                type="text"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                className="border text-sm borger-gray-400 p-2 rounded-md w-full"
                />

                <button className="bg-green-900 text-white py-2 px-5 rounded" id = "loginBtn" onClick={() => login(username,password)}>Login</button>
            </div>
        )}
        
         </>
    )


}