import React from "react";
import ReactDOM from "react-dom";
import { Routes, Route, BrowserRouter } from "react-router-dom";

import Header from "home/Header";
import Footer from "home/Footer";


import "remixicon/fonts/remixicon.css"
import "./index.scss";

import ProductPage from "./ProductPage";

const App = () => (
  <BrowserRouter>
    <div className="text-3xl mx-auto max-w-6xl">
      <Header />
      <div className="my-10">
        {/* <div>    WHat  if home server goes down???
      </div>
    <div>    Deployment process: Home is build and deployed in static store like s3 
      </div>
      <div>    1. npm build 2. cd ./dist 3. PORT=3000 npx servor 
      </div> */}
      {/* Home is remote
      Details page is host 
      Details is hosting headers that is remote to it and remoted in from home page     */}
        <Routes>
          <Route path="/products/:id" element={<ProductPage />} />
        </Routes>
      </div>
      <Footer />
    </div>
  </BrowserRouter>
  // <BrowserRouter>
  // <div className="my-10">
  //       <Routes>
  //         <Route path="/products/:id" element={<ProductPage />} />
  //       </Routes>
  //     </div>
  //     </BrowserRouter>
);
ReactDOM.render(<App />, document.getElementById("app"));
