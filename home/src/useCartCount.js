import {useEffect, useState}  from 'react';
import cart from 'cart/cart';

export function useCartCount(){
    const [count, setCount]  = useState(cart?.cartItems?.length);

    useEffect(() =>{
        async function abc() {
      return cart.subscribe((c) => {
        setCount(c?.cartItems.length);
      });
    }

    abc();
     //   cart?.subscribe(({cartItems}) => setCount(cartItems.length))
    },[])
    return count;
}
