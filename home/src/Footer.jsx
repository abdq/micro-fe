import React from 'react';
import "./index.scss";

export default function Footer(){
    return(
        <div className='p-5 bg-green-500 text-white text-3xl font-bold'>
            My Footer
        </div>
    )
}