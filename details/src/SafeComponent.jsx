import React from 'react'

export default class SafeComponont extends React.Component  {
    constructor(props){
        super(props);
        this.state = { hasError: false };

    }

    static getDerivedStateFromError(error){
        console.log("error", error);
        return {hasError: true}
    }
    

    componentDidCatch(){
        console.log("componentDidCatch");
        this.setState({hasError: true})
        return {hasError: true}
    }
    render(){
            if(this.state.hasError){
                return <h1>Something went wrong</h1>
            }
            return this.props.children;
        }
}